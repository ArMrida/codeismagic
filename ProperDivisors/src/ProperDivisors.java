public class ProperDivisors {

	public static void main(String[] args) {
		int k = 2;
		int m = 80;
		int n = 4;
		int result = 0;
		for (int i = k; i < m; i++) {
			if (properDivs(i) == n) {
				result++;
				// System.out.print("Number: "+i+" ");
				// System.out.println("Divs: "+properDivs(i));
			}
		}
		System.out.println(result);

	}

	public static int properDivs(int n) {
		int divs = 0;
		if (n == 1) {
			return divs;
		}
		for (int x = 2; x < n; x++) {
			if (n % x == 0) {
				divs++;
			}
		}
		return divs;
	}

}
