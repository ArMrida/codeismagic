import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class FibonacciLists {

	public static void main(String[] args) {
		List<Integer> fibonNumbersTo30 = new ArrayList<>();
		fibonNumbersTo30.add(0);
		fibonNumbersTo30.add(1);
		for (int i = 2; i < 30; i++) {
			int fibonValue = fibonNumbersTo30.get(i - 2) + fibonNumbersTo30.get(i - 1);
			fibonNumbersTo30.add(fibonValue);
		}
		List<Integer> firstHalf = new ArrayList();
		firstHalf = fibonNumbersTo30.subList(0, 15);
		List<Integer> secondHalf = new ArrayList();
		secondHalf = fibonNumbersTo30.subList(15, 30);
		List<Integer> evenNumbers = new ArrayList();
		List<Integer> oddNumbers = new ArrayList();
		for (int iterator : fibonNumbersTo30) {
			if (iterator % 2 == 0) {
				evenNumbers.add(iterator);
			} else {
				oddNumbers.add(iterator);
			}
		}

		System.out.println("Fibonacci numbers:");
		System.out.println(Arrays.toString(fibonNumbersTo30.toArray()));
		System.out.println("First half:");
		System.out.println(Arrays.toString(firstHalf.toArray()));
		System.out.println("Second half:");
		System.out.println(Arrays.toString(secondHalf.toArray()));
		System.out.println("Even numbers:");
		System.out.println(Arrays.toString(evenNumbers.toArray()));
		System.out.println("Odd numbers:");
		System.out.println(oddNumbers);
	}

}
