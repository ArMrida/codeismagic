import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

class Player {
	private String name;

	public Player(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}

public class Texas_holdem {

	public static void main(String[] args) {
		List<String> cardColorList = Arrays.asList("Hearts", "Tiles", "Clovers", "Pikes");
		List<String> cardTypeList = Arrays.asList("2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King",
				"Ace");
		List<String> drawnCardsList = new ArrayList<String>();
		// Set<String> Jenny = new HashSet<String>();
		// Set<String> Bob = new HashSet<String>();
		Set<String> Table = new HashSet<String>();
		generatePlayerDecks(cardColorList, cardTypeList, drawnCardsList);
		// fillDeckWithRandomCards(cardColorList, cardTypeList, Jenny, drawnCardsList);
		// fillDeckWithRandomCards(cardColorList, cardTypeList, Bob, drawnCardsList);
		fillTableWithRandomCards(cardColorList, cardTypeList, Table, drawnCardsList);
		/*
		 * System.out.println("Jenny's hand: ");
		 * System.out.println(Arrays.toString(Jenny.toArray()));
		 * System.out.println("Bob's hand: ");
		 * System.out.println(Arrays.toString(Bob.toArray()));
		 */
		System.out.println("Table: ");
		System.out.println(Arrays.toString(Table.toArray()));

	}

	private static void fillDeckWithRandomCards(List<String> cardColorList, List<String> cardTypeList,
			Set<String> playerName, List<String> drawnCardsList) {
		for (int i = 0; i < 2; i++) {
			String card = generateRandomCard(cardColorList, cardTypeList);
			while (drawnCardsList.contains(card)) {
				card = generateRandomCard(cardColorList, cardTypeList);
			}
			playerName.add(card);
			drawnCardsList.add(card);
		}
	}

	private static void fillTableWithRandomCards(List<String> cardColorList, List<String> cardTypeList,
			Set<String> table, List<String> drawnCardsList) {
		for (int i = 0; i < 5; i++) {
			String card = generateRandomCard(cardColorList, cardTypeList);
			while (drawnCardsList.contains(card)) {
				card = generateRandomCard(cardColorList, cardTypeList);
			}
			table.add(card);
			drawnCardsList.add(card);
		}
	}

	private static String generateRandomCard(List<String> cardColorList, List<String> cardTypeList) {
		String randomCard = getRandomColor(cardColorList) + " " + getRandomCard(cardTypeList);
		return randomCard;
	}

	public static String getRandomColor(List<String> cardColorList) {

		Random rand = new Random();
		String randomColor = cardColorList.get(rand.nextInt(cardColorList.size()));
		return randomColor;
	}

	public static String getRandomCard(List<String> cardTypeList) {

		Random rand = new Random();
		String randomCard = cardTypeList.get(rand.nextInt(cardTypeList.size()));
		return randomCard;
	}

	public static List<String> getPlayerNames() {
		Scanner scan = new Scanner(System.in);
		boolean error = false;
		int playerNum = 0;
		do {
			System.out.println("Number of players: ");
			playerNum = scan.nextInt();
			if ((playerNum < 2) || (playerNum > 7)) {
				System.out.println("Number of players must be between 2 and 7!");
				error = true;
			} else {
				error = false;
			}
		} while (error == true);
		List<String> playerNames = new ArrayList<String>();

		Scanner scan2 = new Scanner(System.in);
		for (int i = 1; i < playerNum + 1; i++) {
			System.out.println("Player #" + i + ":");
			String player = scan2.nextLine();
			playerNames.add(player);
		}
		scan.close();
		scan2.close();
		return playerNames;
	}

	public static void generatePlayerDecks(List<String> cardColorList, List<String> cardTypeList,
			List<String> drawnCardsList) {
		List<String> playerNames = getPlayerNames();
		for (String player : playerNames) {
			Set<String> newPlayer = new HashSet<String>();
			fillDeckWithRandomCards(cardColorList, cardTypeList, newPlayer, drawnCardsList);
			System.out.println(player + "' hand:");
			System.out.println(Arrays.toString(newPlayer.toArray()));
		}

	}

}
