import java.util.Arrays;
import java.util.List;

public class AVGspeed {

	public static void main(String[] args) {
		List<Double> measuredSpeedDataSet = Arrays.asList(12.30, 12.45, 12.35, 12.54, 12.53, 12.45, 12.61, 12.62, 12.73,
				12.91, 12.90, 12.72, 12.85, 12.90, 12.70, 12.58, 12.61, 12.67, 12.63, 12.61, 12.74, 12.84, 12.99, 13.10,
				13.21, 13.39, 13.23, 13.09, 13.15, 13.00, 12.91, 12.85, 12.89, 13.00, 13.03, 13.16, 13.25, 13.35, 13.36,
				13.28, 13.42, 13.49, 13.36, 13.32, 13.20, 13.23, 13.20, 13.02, 12.91, 12.74, 12.81, 12.63, 12.79, 12.65,
				12.59, 12.77, 12.77, 12.89, 12.76, 12.76, 12.91, 12.90, 12.86, 12.89, 12.83, 12.67, 12.69, 12.65, 12.73,
				12.87, 12.89, 12.82, 12.92, 13.06, 13.16, 13.09, 13.15, 13.19, 13.17, 13.19, 12.99, 13.09, 13.17, 13.17,
				13.00, 12.89, 12.72, 12.70, 12.87, 12.83, 12.64, 12.61, 12.50, 12.44, 12.26, 12.40, 12.57, 12.47, 12.58,
				12.56, 12.62, 12.62, 12.57, 12.57, 12.51, 12.34, 12.18, 12.34, 12.26, 12.46, 12.45, 12.53, 12.65, 12.82,
				12.93, 13.02, 13.01, 12.92, 12.94, 12.98, 13.17, 13.06, 13.12, 13.23, 13.28, 13.33, 13.23, 13.17, 13.25,
				13.23, 13.16, 13.23, 13.41, 13.28, 13.34, 13.45, 13.29, 13.23, 13.27, 13.38, 13.25, 13.32, 13.19, 13.10,
				12.91, 12.84, 12.98, 12.95, 12.79, 12.75, 12.84, 12.80, 12.79, 12.97, 12.88, 13.01, 12.91, 12.80, 12.73,
				12.72, 12.71, 12.61, 12.65, 12.85, 12.72, 12.90, 12.73, 12.90, 13.01, 13.19, 13.04, 12.95, 13.14, 13.03,
				13.02, 12.82, 12.97, 12.83, 12.95, 12.87, 12.77, 12.90, 13.08, 13.21, 13.10, 12.96, 13.10, 13.26, 13.29,
				13.25, 13.30, 13.24, 13.30, 13.20, 13.11, 13.16, 13.06, 13.25, 13.44, 13.49);
		int sum = 0;
		Double min = measuredSpeedDataSet.get(0);
		Double max = measuredSpeedDataSet.get(0);
		for (Double number : measuredSpeedDataSet) {
			sum += number;
			if (min > number) {
				min = number;
			}
			if (max < number) {
				max = number;
			}
		}
		System.out.println("Average speed is: " + (double)(sum / measuredSpeedDataSet.size()));
		System.out.println("Minimum speed is: " + min);
		System.out.println("Maximum speed is: " + max);

	}
}
