import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Book_donation {

	public static void main(String[] args) {
		List<String> donation = Arrays.asList("Son", "Harry Potter VI.", "Down and Out in Paris and London",
				"All about Sam", "1984", "Keep the Aspidistra Flying", "The Fellowship of the Ring",
				"Keep the Aspidistra Flying", "A Summer to Die", "Harry Potter V", "The Return of the King",
				"Harry Potter VI.", "The Return of the King", "Down and Out in Paris and London", "Harry Potter V",
				"A Summer to Die", "Harry Potter I.", "Harry Potter III.", "All about Sam", "Animal Farm",
				"Gathering Blue", "Homage to Catalonia", "Son", "The Two Towers", "Harry Potter III.", "Messenger",
				"The Return of the King", "Homage to Catalonia", "Harry Potter III.", "Harry Potter I.",
				"The Two Towers", "Gathering Blue", "Messenger", "The Fellowship of the Ring", "1984",
				"Harry Potter VI.", "Keep the Aspidistra Flying", "Gathering Blue", "Harry Potter V",
				"Harry Potter II.", "Homage to Catalonia", "Harry Potter V", "Animal Farm", "All about Sam", "Son");
		Set<String> books = new HashSet(donation);
		Map<String, Integer> hm = new HashMap();

		for (String book : donation) {

			if (!hm.containsKey(book)) {
				hm.put(book, 1);
			} else {
				hm.put(book, hm.get(book) + 1);
			}
		}
		System.out.println("We have " + books.size() + " different book titles, namely:");
		System.out.println(Arrays.toString(books.toArray()));
		System.out.println("The list of all books in store: ");
		System.out.println(hm);
	}

}
