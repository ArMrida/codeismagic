import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SideEffect {

	public static void main(String[] args) {
		List<String> cards = Arrays.asList("Pikes 3", "Hearts Jack", "Tiles King");
		List<String> shuffledItems = shuffle(cards);
		System.out.println(cards);
		System.out.println(shuffledItems);
		
	}

	public static List<String> shuffle(List<String> items) {
		List<String> copy = new ArrayList<>(items);
		Collections.shuffle(copy);
		return copy;
	}
}
