import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class WrongCardDeck {

	public static void main(String[] args) {
		List<String> A = Arrays.asList("Tiles 6", "Clovers 8", "Hearts 5", "Hearts Jack", "Tiles 8", "Hearts 2",
				"Clovers 4", "Tiles 7", "Clovers Ace", "Clovers 2", "Pikes Jack", "Hearts 9", "Pikes Queen",
				"Clovers Jack", "Hearts King", "Clovers 3", "Pikes 3", "Hearts 4", "Pikes 7", "Clovers 7",
				"Tiles Queen", "Clovers 9", "Tiles 10", "Clovers 5", "Pikes 4", "Hearts 10", "Pikes Ace", "Tiles 9",
				"Tiles 3", "Pikes 5", "Tiles Jack", "Clovers 6", "Hearts 6", "Hearts Ace", "Tiles King", "Clovers King",
				"Clovers Queen", "Hearts 8", "Tiles 5", "Tiles 4", "Pikes 8", "Hearts 3", "Pikes 9", "Pikes 6",
				"Hearts Queen", "Tiles Ace", "Pikes King", "Tiles 2", "Clovers 10", "Hearts 7", "Pikes 2", "Pikes 10");
		List<String> B = Arrays.asList("Pikes Ace", "Clovers 3", "Hearts Ace", "Clovers Queen", "Hearts King",
				"Tiles 10", "Pikes King", "Pikes 3", "Tiles 8", "Tiles Jack", "Tiles Ace", "Tiles 6", "Clovers Ace",
				"Hearts 10", "Clovers 7", "Pikes Queen", "Pikes 10", "Pikes 5", "Clovers King", "Tiles 3", "Hearts 5",
				"Hearts 6", "Pikes 9", "Clovers 5", "Clovers 2", "Tiles 5", "Tiles 9", "Hearts 4", "Hearts 3",
				"Hearts 8", "Hearts Queen", "Tiles Queen", "Tiles 4", "Clovers 6", "Pikes 2", "Pikes 4", "Tiles King",
				"Tiles 2", "Clovers 9", "Clovers Jack", "Tiles 7", "Pikes 8", "Hearts 7", "Hearts Jack", "Clovers 8",
				"Pikes 7", "Hearts 9", "Pikes 6", "Clovers 4", "Pikes Jack", "Hearts 2", "Clovers 10");
		List<String> C = Arrays.asList("Tiles 8", "Hearts 2", "Pikes 8", "Tiles 4", "Clovers 9", "Tiles 3", "Hearts 4",
				"Clovers 4", "Pikes Ace", "Clovers 10", "Tiles Queen", "Clovers Jack", "Hearts Queen", "Hearts 6",
				"Hearts 8", "Tiles 5", "Clovers 2", "Tiles 10", "Hearts 10", "Tiles Jack", "Pikes Queen", "Tiles 2",
				"Clovers 5", "Hearts 3", "Pikes 7", "Clovers Ace", "Tiles 6", "Clovers 6", "Clovers 3", "Pikes 10",
				"Clovers Queen", "Clovers King", "Pikes 6", "Hearts Jack", "Pikes 3", "Pikes 9", "Hearts King",
				"Tiles Ace", "Pikes 5", "Hearts Ace", "Pikes King", "Hearts 9", "Clovers 8", "Hearts 5", "Clovers 7",
				"Tiles 9", "Pikes Jack", "Pikes 4", "Hearts 7", "Tiles King", "Pikes 2", "Tiles 7");
		List<String> D = Arrays.asList("Clovers King", "Pikes 2", "Hearts 3", "Pikes 8", "Pikes Ace", "Clovers Jack",
				"Pikes 7", "Tiles 3", "Tiles 5", "Clovers 4", "Pikes King", "Tiles 8", "Tiles 4", "Tiles 6", "Pikes 10",
				"Clovers 10", "Clovers 7", "Clovers 3", "Tiles Jack", "Pikes Jack", "Hearts 4", "Tiles Ace",
				"Clovers 5", "Pikes 3", "Hearts King", "Tiles 9", "Clovers 9", "Tiles 10", "Clovers 8", "Hearts 8",
				"Tiles Queen", "Clovers Queen", "Clovers 6", "Pikes 5", "Hearts 6", "Hearts 5", "Pikes 6", "Hearts 2",
				"Tiles 7", "Clovers 2", "Tiles King", "Hearts 7", "Hearts Queen", "Hearts Ace", "Tiles 2",
				"Hearts Jack", "Hearts 9", "Pikes 9", "Clovers Ace", "Hearts 10", "Pikes Queen", "Tiles 7");
		List<String> E = Arrays.asList("Clovers King", "Pikes Jack", "Hearts Ace", "Clovers 9", "Clovers 3",
				"Clovers 5", "Pikes 2", "Tiles 7", "Pikes Queen", "Tiles King", "Tiles Queen", "Clovers 10", "Tiles 8",
				"Pikes Ace", "Hearts 5", "Pikes 9", "Tiles 9", "Clovers Queen", "Tiles 4", "Pikes 3", "Tiles 5",
				"Clovers Ace", "Tiles 6", "Hearts 9", "Hearts 10", "Pikes 6", "Pikes King", "Tiles Ace", "Clovers 2",
				"Pikes 10", "Tiles 10", "Tiles 3", "Hearts 3", "Pikes 4", "Hearts 4", "Pikes 8", "Clovers 6", "Tiles 2",
				"Hearts 8", "Hearts 2", "Hearts Queen", "Pikes 7", "Hearts 6", "Clovers 4", "Clovers 7", "Tiles Jack",
				"Clovers Jack", "Hearts Jack", "Hearts King", "Clovers 8", "Hearts 7", "Pikes 5");
		List<String> F = Arrays.asList("Pikes 4", "Tiles 3", "Clovers Queen", "Hearts 7", "Pikes King", "Pikes Jack",
				"Clovers King", "Hearts 10", "Pikes Queen", "Hearts King", "Tiles 5", "Hearts 4", "Hearts 3", "Pikes 5",
				"Tiles Queen", "Tiles 9", "Clovers 2", "Clovers Ace", "Hearts Queen", "Tiles 4", "Tiles 6",
				"Hearts Jack", "Hearts 5", "Clovers 4", "Pikes 8", "Tiles 8", "Pikes 6", "Pikes 2", "Tiles 7",
				"Pikes 7", "Clovers 7", "Tiles Jack", "Clovers 3", "Tiles Ace", "Hearts 9", "Clovers 6", "Clovers 8",
				"Tiles King", "Tiles 2", "Hearts 8", "Clovers 10", "Hearts Ace", "Pikes 10", "Hearts 6", "Clovers Jack",
				"Tiles 10", "Pikes Ace", "Hearts 2", "Pikes 9", "Clovers 5", "Clovers 9", "Pikes 3");
		List<String> cardColorList = Arrays.asList("Hearts", "Tiles", "Clovers", "Pikes");
		List<String> cardTypeList = Arrays.asList("2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King",
				"Ace");
		List<String> drawnCardsList = new ArrayList<String>();
		drawAllValidCards(cardColorList, cardTypeList, drawnCardsList);
		validateDeck(D, drawnCardsList);

	}

	public static void validateDeck(List<String> deck, List<String> drawnCardList) {
		Set<String> validationSet = new HashSet<String>();
		List<String> duplicates = new ArrayList();
		for (String card : deck) {
			if (validationSet.contains(card)) {
				duplicates.add(card);
			}
			validationSet.add(card);
		}
		if (deck.size() != validationSet.size()) {
			System.out.println("This deck is not valid!");
			System.out.println("Duplicates: ");
			System.out.println(Arrays.toString(duplicates.toArray()));
			for (String card : drawnCardList) {
				if (!validationSet.contains(card)) {
					System.out.println(card+" is missing from the deck.");
				}
			}
		} else {
			System.out.println("Congrats, this deck is valid!");
		}
	}

	private static void drawAllValidCards(List<String> cardColorList, List<String> cardTypeList,
			List<String> drawnCardsList) {
		for (int i = 0; i < 52; i++) {
			String card = generateRandomCard(cardColorList, cardTypeList);
			while (drawnCardsList.contains(card)) {
				card = generateRandomCard(cardColorList, cardTypeList);
			}
			drawnCardsList.add(card);
		}
	}

	private static String generateRandomCard(List<String> cardColorList, List<String> cardTypeList) {
		String randomCard = getRandomColor(cardColorList) + " " + getRandomCard(cardTypeList);
		return randomCard;
	}

	public static String getRandomColor(List<String> cardColorList) {

		Random rand = new Random();
		String randomColor = cardColorList.get(rand.nextInt(cardColorList.size()));
		return randomColor;
	}

	public static String getRandomCard(List<String> cardTypeList) {

		Random rand = new Random();
		String randomCard = cardTypeList.get(rand.nextInt(cardTypeList.size()));
		return randomCard;
	}
}
