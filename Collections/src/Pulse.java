import java.util.Arrays;
import java.util.List;

public class Pulse {

	public static void main(String[] args) {
		List<Integer> measuredHeartRateDataSet = Arrays.asList(157, 159, 161, 158, 158, 161, 158, 159, 159, 156, 151,
				154, 153, 153, 154, 157, 156, 156, 154, 155, 158, 161, 159, 158, 158, 161, 160, 160, 158, 161, 161, 163,
				160, 160, 160, 162, 161, 161, 162, 162, 161, 158, 158, 156, 159, 160, 160, 160, 159, 159, 161, 161, 161,
				162, 160, 159, 156, 157, 160, 160, 162, 163, 163, 161, 159, 156, 158, 156, 156, 159, 161, 161, 163, 163,
				163, 163, 165, 163, 165, 168, 168, 167, 164, 162, 161, 162, 164, 166, 163, 164, 165, 165, 164, 161, 162,
				165, 162, 163, 163, 163, 161, 158, 158, 160, 161, 161, 162, 159, 161, 162, 159, 162, 164, 167, 164, 167,
				167, 164, 165, 165, 167, 166, 166, 166, 168, 168, 169, 169, 169, 167, 167, 167, 164, 163, 164, 163, 163,
				164, 165, 167, 169, 171, 174, 175, 178, 178, 176, 173, 176, 176, 176, 173, 171, 168, 170, 170, 172, 171,
				174, 174, 175, 172, 171, 168, 168, 165, 165, 163, 161, 161, 161, 159, 160, 159, 158, 155, 154, 154, 154,
				151, 152, 155, 157, 155, 155, 158, 160, 160, 163, 162, 163, 161, 161, 163, 162, 163, 165, 165, 167, 170,
				167, 167, 166, 166, 165, 164, 164, 163, 160, 160, 158, 157, 159, 159, 156, 159, 158, 155, 155, 153, 154,
				156, 158, 158, 156, 153, 155, 154, 155, 155, 157, 157, 157, 160, 160, 159, 162, 161, 162, 162, 159, 159,
				162, 161, 160, 160, 161, 159, 162, 159);

		int min = measuredHeartRateDataSet.get(0);
		int max = measuredHeartRateDataSet.get(0);
		int E1 = 0;
		int E2 = 0;
		int EX = 0;
		int AN = 0;
		int SN = 0;
		int measuredHeartRateDataSetSize = measuredHeartRateDataSet.size();
		for (int heartRate : measuredHeartRateDataSet) {
			if (min > heartRate) {
				min = heartRate;
			} else if (max < heartRate) {
				max = heartRate;
			}
			if (heartRate >= 0 && heartRate <= 158) {
				E1++;
			} else if (heartRate >= 159 && heartRate <= 168) {
				E2++;
			} else if (heartRate >= 169 && heartRate <= 178) {
				EX++;
			} else if (heartRate >= 179 && heartRate <= 188) {
				AN++;
			} else if (heartRate >= 189) {
				SN++;
			}
		}
		System.out.println("Min: " + min);
		System.out.println("Max: " + max);
		System.out.println("E1: " + calculatePercent(measuredHeartRateDataSetSize, E1));
		System.out.println("E2: " + calculatePercent(measuredHeartRateDataSetSize, E2));
		System.out.println("EX: " + calculatePercent(measuredHeartRateDataSetSize, EX));
		System.out.println("AN: " + calculatePercent(measuredHeartRateDataSetSize, AN));
		System.out.println("SN: " + calculatePercent(measuredHeartRateDataSetSize, SN));
		// System.out.println("E1: "+E1+" E2: "+E2+" EX: "+EX+" AN: "+ AN+" SN: "+ SN);

	}

	static float calculatePercent(int n, int v) {
		float percent = (v * 100.0f) / n;
		return percent;
	}
}
