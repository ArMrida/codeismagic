import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Dalek_army {

	public static void main(String[] args) {
		System.out.println("Number of daleks: ");
		Scanner scan = new Scanner(System.in);
		int armySize = scan.nextInt();
		String[] DalekArmy = new String[armySize];
		for (int i = 0; i < armySize; i++) {
			long generatedLong = Math.abs(new Random().nextLong());
			DalekArmy[i] = "Dalek-" + generatedLong;
		}
		for (String name : DalekArmy) {
			System.out.println(name);
		}

	}
}
