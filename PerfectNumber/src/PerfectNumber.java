public class PerfectNumber {

	public static void main(String[] args) {
		isPerfectNumber(5);

	}
	public static void isPerfectNumber(int number){
        
        int divisors = 0;
        for(int i=1;i<=number/2;i++){
            if(number%i == 0){
                divisors += i;
            }
        }
        if(divisors == number){
            System.out.println("The number is perfect");
            
        } else {
            System.out.println("The number is NOT perfect");
            
        }
    }

}
