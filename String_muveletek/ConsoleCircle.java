package codeismagic.stringprocessing;

public class ConsoleCircle {

	public static void main(String[] args) {
		StringBuilder circle = new StringBuilder();
		for (int row = 0; row < 20; row++) {
			for (int column = 0; column < 20; column++) {
				if (Math.sqrt((10 - row) * (10 - row) + (10 - column) * (10 - column)) < 6.5) {
					circle.append("O0");
				} else {
					circle.append("--");
				}

			}
			circle.append(System.lineSeparator());
		}
		System.out.println(circle);
	}

}
