package codeismagic.stringprocessing;

import java.util.Scanner;

public class Elevator {

	public static void main(String[] args) {
		drawAnEmptyHouse();
		int floor = scanFloorFromConsole();

		StringBuilder elevator = new StringBuilder();
		for (int i = 0; i < 6 - floor - 1; i++) {
			drawAFloor(elevator);
		}

		drawAFloorWithSmiley(elevator);
		for (int i = 0; i < floor; i++) {
			drawAFloor(elevator);
		}
		System.out.println(elevator);
	}

	private static int scanFloorFromConsole() {
		int floor = 0;
		boolean wrongFloor = false;
		do {
			System.out.println("Which floor do you want to go to?");
			Scanner scan = new Scanner(System.in);
			floor = scan.nextInt();

			if ((floor < 0) || (floor > 6)) {
				System.out.println("Wrong floor! Please enter a number between 0 (basement) and 6!");
				wrongFloor = true;
			} else {
				wrongFloor = false;
			}
		} while (wrongFloor == true);
		return floor;
	}

	private static void drawAnEmptyHouse() {
		StringBuilder sb = new StringBuilder();
		for (int floor = 0; floor < 6; floor++) {
			drawAFloor(sb);
		}
		System.out.println(sb);
	}

	private static void drawAFloor(StringBuilder sb) {
		sb.append("------");
		sb.append(System.lineSeparator());
		sb.append("|    |");
		sb.append(System.lineSeparator());
	}

	private static void drawAFloorWithSmiley(StringBuilder sb) {
		sb.append("------");
		sb.append(System.lineSeparator());
		sb.append("| :) |");
		sb.append(System.lineSeparator());
	}

}
