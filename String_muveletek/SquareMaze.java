package codeismagic.stringprocessing;

import java.util.Random;
import java.util.Scanner;

public class SquareMaze {
// kérdés: hogyan lehetne a scan részt metódusba kiszervezni?
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Number of rows: ");
		int rows = scan.nextInt();
		System.out.println("Number of columns: ");
		int columns = scan.nextInt();
		System.out.println("Probability: ");
		int probability = scan.nextInt();
		StringBuilder sb = new StringBuilder();
		Random random = new Random();

		for (int i = 0; i < rows; i++) {

			for (int j = 0; j < columns; j++) {
				int probabilityOfEmptySpace = random.nextInt(101);
				if (probabilityOfEmptySpace > probability) {
					sb.append("| ");
				} else {
					sb.append("|*");
				}
				
			}
			sb.append("|");
			sb.append(System.lineSeparator());
		}

		System.out.println(sb);

	}

}

/*
 * 
 */