package codeismagic.stringprocessing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class LearningTracker {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String command = scan.nextLine();
		String learn_type = "";
		String learn_duration = "";
		List<String> learn_list = new ArrayList<>();
		while (!command.equals("exit")) {
			command = scan.nextLine();
			if (command.equals("learn")) {

				System.out.println("Learning type: ");
				learn_type = scan.nextLine();
				System.out.println("Duration in minutes: ");
				learn_duration = scan.nextLine();
				learn_list.add(learn_type + "," + learn_duration);
				System.out.println("Good! I saved your activity :)");
			} else if (command.equals("learn-list")) {
				for (String value:learn_list) {
					System.out.println(value);
				}
			}
			else if (command.equals("learn-stat")) {
				// System.out.println(makestats(learn_list));
			}
		}
		scan.close();

	}

	/*private static Map<Set<String, Integer>, Integer> makestats(List<String> learn_list) {
		Map<Set<String, Integer>, Integer> learnStats=new HashMap<>();
		for (String element:learn_list) {
			int colon = element.indexOf(",");
			String learn_type=element.substring(0, colon);
			String learn_duration=element.substring(colon);
			if (learnStats.containsKey(arg0))
		}
		return learnStats;
		
	}*/

}
