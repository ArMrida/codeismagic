package codeismagic.stringprocessing;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Startup_share {

	public static void main(String[] args) {
		List<String> loggedHours = Arrays.asList(
			    "Zuckerberg,4", "Steve,4", "Bill,1", "Bill,2", "Zuckerberg,4", "Bill,2",
			    "Zuckerberg,1", "Zuckerberg,3", "Zuckerberg,1", "Zuckerberg,1", "Steve,4",
			    "Bill,2", "Zuckerberg,3", "Bill,2", "Zuckerberg,4", "Steve,1",
			    "Bill,3", "Steve,2", "Steve,2", "Bill,1", "Zuckerberg,4",
			    "Bill,4", "Steve,4", "Bill,2", "Bill,3", "Zuckerberg,3",
			    "Steve,4", "Zuckerberg,4", "Bill,2", "Zuckerberg,3"
			);

System.out.println(mapLoggedHours(loggedHours));
calculatePercentage(mapLoggedHours(loggedHours));
	}

	private static void calculatePercentage(Map<String, Integer> workHoursByName) {
		int sum=0;
		double percent=0;
		for (int value : workHoursByName.values()) {
		    sum+=value;
		}
		for (Map.Entry<String, Integer> entry : workHoursByName.entrySet()) {
		    String key = entry.getKey();
		    int value = entry.getValue();
		    percent=(double)value/sum*100;
		    System.out.println(key+": "+percent+"%");
		}
	}

	private static Map<String, Integer> mapLoggedHours(List<String> loggedHours) {
		Map<String, Integer> workHoursByName = new HashMap<>();
		String key ="";
		String value ="";
		for(String element : loggedHours) {
			String[] name_hour_pair = element.trim().split(",");
			key=name_hour_pair[0];
			value=name_hour_pair[1];
			
			if (!workHoursByName.containsKey(key)) {
				workHoursByName.put(key, Integer.parseInt(value));
			}
			else {
				workHoursByName.put(key, workHoursByName.get(key) +Integer.parseInt(value) );
			}
		}
		return workHoursByName;
		
		
	}

}
