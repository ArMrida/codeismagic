package codeismagic.stringprocessing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Uppercase_names {

	public static void main(String[] args) {
		List<String> namesInput = Arrays.asList("Bob", "Adam", "Dave", "Jenny", "Scarlett", "Vanda", "Ann", "Jenna",
				"Bill", "Steve", "Kelly", "Angelica", "Armstrong");
		List<String> namesToUpperCase = new ArrayList<>();
		for (String name:namesInput) {
			namesToUpperCase.add(name.toUpperCase());
		}
		System.out.println(namesToUpperCase);
	}

}
