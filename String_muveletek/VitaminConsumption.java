package codeismagic.stringprocessing;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;

public class VitaminConsumption {

	public static void main(String[] args) {
		List<String> vitaminConsumptions = Arrays.asList("Name:Scarlett-6fccbdca,C:50,D:600,E:3000",
				"Name:Scarlett-c52ec32f,C:350,D:2000,E:4000", "Name:Bob-600b03a6,C:150,D:2000,E:2200",
				"Name:Scarlett-38d7e3a2,C:150,D:1000,E:100", "Name:Scarlett-3abde882,C:950,D:1200,E:300",
				"Name:Scarlett-76b92994,C:900,D:1600,E:4000", "Name:Bob-2b4a1544,C:600,D:800,E:1800",
				"Name:Bob-2421990a,C:50,D:200,E:1200", "Name:Bob-dd9be590,C:800,D:1200,E:600",
				"Name:Bob-4e0104b2,C:850,D:1000,E:2400", "Name:Bob-d63cb4ae,C:300,D:1800,E:200",
				"Name:Scarlett-294ece9c,C:100,D:1600,E:3100", "Name:Bob-a2a8b3d4,C:300,D:1200,E:1000",
				"Name:Bob-b87ae9dc,C:800,D:2000,E:2500", "Name:Scarlett-8e174152,C:350,D:200,E:3800",
				"Name:Scarlett-ec5a81be,C:450,D:1600,E:3700", "Name:Scarlett-ef7b71b9,C:50,D:400,E:1500",
				"Name:Scarlett-97856f0d,C:1000,D:1200,E:2000", "Name:Bob-2883e779,C:950,D:800,E:4000",
				"Name:Scarlett-abcc50bc,C:550,D:600,E:2800", "Name:Scarlett-28af295b,C:650,D:800,E:500",
				"Name:Bob-4f501360,C:300,D:1000,E:200", "Name:Scarlett-b651419b,C:700,D:600,E:2300",
				"Name:Scarlett-849ed217,C:700,D:400,E:1400", "Name:Bob-9df7af78,C:400,D:800,E:2900",
				"Name:Bob-4abfcbf4,C:900,D:2000,E:1800", "Name:Bob-12861bf3,C:500,D:800,E:3500",
				"Name:Bob-64acb8de,C:400,D:600,E:1900", "Name:Bob-4ff62d7d,C:400,D:2000,E:3100",
				"Name:Scarlett-bb59ec99,C:200,D:1000,E:1400", "Name:Scarlett-beb47f0b,C:900,D:1800,E:3000",
				"Name:Scarlett-31f9c133,C:350,D:800,E:2000", "Name:Bob-da67b398,C:550,D:800,E:1900",
				"Name:Scarlett-20db85c6,C:450,D:200,E:2200", "Name:Scarlett-d9e48955,C:200,D:1000,E:800",
				"Name:Scarlett-7da8f2aa,C:200,D:200,E:1600", "Name:Scarlett-f351e4d0,C:700,D:200,E:300",
				"Name:Bob-3d58e4c1,C:700,D:800,E:1700", "Name:Bob-0b8301b7,C:150,D:1400,E:1000",
				"Name:Bob-4041df3e,C:900,D:1800,E:1700", "Name:Scarlett-3e15502a,C:450,D:600,E:2200",
				"Name:Scarlett-1fae31bc,C:950,D:800,E:1800", "Name:Bob-47126df2,C:1000,D:1000,E:1900",
				"Name:Scarlett-63167044,C:550,D:1400,E:1600", "Name:Scarlett-ea01d4c2,C:800,D:800,E:3000",
				"Name:Scarlett-1c0c8bd5,C:1000,D:800,E:4000", "Name:Scarlett-af993b7d,C:800,D:1400,E:500",
				"Name:Scarlett-dc49a3f7,C:500,D:800,E:1200", "Name:Bob-2c2d260b,C:600,D:1200,E:2600",
				"Name:Bob-a4e7e3f7,C:150,D:1800,E:3600", "Name:Scarlett-991f6f64,C:750,D:2000,E:4000");
		writeWarnings(vitaminConsumptions);

	}

	private static void writeWarnings(List<String> vitaminConsumptions) {
		int cRecommendation = 500;
		int dRecommendation = 800;
		int eRecommendation = 100;
		for (String patient : vitaminConsumptions) {
			int firstColonPosition = patient.indexOf(":");
			int firstCommaPosition = patient.indexOf(",");
			String patientName = patient.substring(firstColonPosition+1, firstCommaPosition);
			String[] vitamins = patient.substring(firstCommaPosition).split(",");
			System.out.println(patientName);
			System.out.println(vitamins);
			int[] vitaminDoses = null;
			/*for (int i = 0; i < vitamins.length; i++) {
				vitaminDoses[i] = Integer.parseInt(vitamins[i].substring(1));
			}
			if (vitaminDoses[0] < cRecommendation) {
				System.out.println(patientName + "should take more vitamin C, consumption was " + vitaminDoses[0]
						+ ", recommended daily amount is " + cRecommendation);
			}
			if (vitaminDoses[1] < dRecommendation) {
				System.out.println(patientName + "should take more vitamin D, consumption was " + vitaminDoses[1]
						+ ", recommended daily amount is " + dRecommendation);
			}
			if (vitaminDoses[2] < eRecommendation) {
				System.out.println(patientName + "should take more vitamin E, consumption was " + vitaminDoses[2]
						+ ", recommended daily amount is " + eRecommendation);
			}
		}*/

	}

	}
}
