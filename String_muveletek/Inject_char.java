package codeismagic.stringprocessing;

import java.util.Random;
import java.util.Scanner;

public class Inject_char {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String text;
		char character;
		System.out.println("Please enter a text!");
		text=scan.nextLine();
		System.out.println("Please enter a character!");
		character=scan.next().charAt(0);
		Random random = new Random();
		int randomIndex=random.nextInt(text.length())+1;
		String text_with_char_inserted=text.substring(0, randomIndex)+character+text.substring(randomIndex);
		System.out.println(text_with_char_inserted);
		

	}

}
