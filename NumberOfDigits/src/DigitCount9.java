import java.util.Scanner;

public class DigitCount9 {

	public static void main(String[] args) {
		System.out.println("Please enter a number!");
		Scanner scan = new Scanner(System.in);
		int number = Math.abs(scan.nextInt());
		scan.close();
		int countOfNines = 0;
		
			for (int i = number; i >= 9; i /= 10) {
				// last digit
				int y = (i % 10);
				if (y == 9) {
					countOfNines++;
				}
			}
		
		System.out.println("The number of 9 is: " + countOfNines);

	}
}
