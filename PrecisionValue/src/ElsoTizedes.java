import java.util.Scanner;

public class ElsoTizedes {

	public static void main(String[] args) {
		System.out.print("Please enter a number! ");
		Scanner scan = new Scanner(System.in);
		double number = scan.nextDouble();
		System.out.println("The first decimal number is: " + getFirstdecimal(number));
	}

	private static int getFirstdecimal(double number) {
		int result = (int) Math.floor((number % 1) * 10);
		return result;
	}

}
