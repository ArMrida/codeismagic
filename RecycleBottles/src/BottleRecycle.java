import java.util.Scanner;

public class BottleRecycle {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String button = "";
		int green = 0;
		int black = 0;
		while (!button.equals("x")) {
			System.out.println("Bottle (Green / Black / X for finish)");
			button = scan.nextLine();
			if (button.equals("Green")) {
				green++;
			} else if (button.equals("Black")) {
				black++;
			} else if (!button.equals("x")) {
				System.out.println("Sorry, this bottle is worth nothing to me.");
			}
		}
		scan.close();
		printResult(green, black);
	}

	private static void printResult(int green, int black) {
		int result = (green * 10) + (black * 30);
		System.out.println("Overall reward: " + result + " Ft");

	}

}
