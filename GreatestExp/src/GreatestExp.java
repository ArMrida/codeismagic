import java.util.Scanner;

public class GreatestExp {

	public static void main(String[] args) {
		System.out.print("Please, enter a number! ");
		Scanner scan = new Scanner(System.in);
		int num = scan.nextInt();
		scan.close();
		double result = 0;
		int i = 0;
		while (result < 567) {
			result = Math.pow(num, i);
			i++;
		}
		i--;
		System.out.println("Result: " + result + ", Exponent: " + i);

	}

}
