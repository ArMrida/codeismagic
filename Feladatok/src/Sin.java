import java.util.Iterator;

public class Sin {

	public static void main(String[] args) {
		String star = "";
		int x = 0;
		for (int i = 0; i < 20; i++) {
			for (int j = 0; j < Math.PI; j += 0.15) {
				x = (int) Math.floor(Math.sin(j) * 30);
				for (int k = 0; k < x; k++) {
					star += "*";
					System.out.println(star);
				}
			}
		}

	}
}
