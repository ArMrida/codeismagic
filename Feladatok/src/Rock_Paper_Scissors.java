import java.util.Random;
import java.util.Scanner;

public class Rock_Paper_Scissors {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String button = "";
		int userScore = 0;
		int computerScore = 0;
		while (!button.equals("x")) {
			System.out.println("Guess one (Rock / Paper / Scissors)! Type x to finish the game!)");
			button = scan.nextLine();
			Random random = new Random();
			int rps = random.nextInt(3);
			String computerChoice = "";
			if (rps == 0) {
				computerChoice = "Rock";
			} else if (rps == 1) {
				computerChoice = "Paper";
			} else {
				computerChoice = "Scissors";
			}
			if (!(button.equals("x"))){
			System.out.println("Computer: " + computerChoice);}

			if ((button.equals("Rock")) && (computerChoice.equals("Scissors"))) {
				userScore++;
				System.out.println("You won!");
				printScores(userScore, computerScore);
			} else if ((button.equals("Rock")) && (computerChoice.equals("Paper"))) {
				computerScore++;
				System.out.println("Computer won!");
				printScores(userScore, computerScore);
			} else if ((button.equals("Rock")) && (computerChoice.equals("Rock")))

			{
				System.out.println("Draw!");
				printScores(userScore, computerScore);
			} else if ((button.equals("Paper")) && (computerChoice.equals("Rock"))) {
				userScore++;
				System.out.println("You won!");
				printScores(userScore, computerScore);
			}
			else if ((button.equals("Paper")) && (computerChoice.equals("Scissors"))) {
				computerScore++;
				System.out.println("Computer won!");
				printScores(userScore, computerScore);
			}
			else if ((button.equals("Paper")) && (computerChoice.equals("Paper"))) {
				System.out.println("Draw!");
				printScores(userScore, computerScore);
			}
			else if ((button.equals("Scissors")) && (computerChoice.equals("Paper"))) {
				userScore++;
				System.out.println("You won!");
				printScores(userScore, computerScore);
			}
			else if ((button.equals("Scissors")) && (computerChoice.equals("Rock"))) {
				computerScore++;
				System.out.println("Computer won!");
				printScores(userScore, computerScore);
			}
			else if ((button.equals("Scissors")) && (computerChoice.equals("Scissors"))) {
				System.out.println("Draw");
				printScores(userScore, computerScore);
			}
			else if (!(button.equals("x")) ) {
				System.out.println("That is invalid, so I consider it a cheat. You lost a point.");
				userScore--;
				printScores(userScore, computerScore);
				
			}

		}
		
		scan.close();
	}

	private static void printScores(int userScore, int computerScore) {
		System.out.println("Your score: " + userScore + " Computer's score: " + computerScore);
	}

}
